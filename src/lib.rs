#[macro_export]
macro_rules! eval {
    () => {};
    (print($($x:expr),*)) => {
        $(print!("{}", $x);)*
        print!("\n");
    };
    (repeat($times:expr, $callback:expr)) => {
        let times: i32 = $times;
        let callback: fn(i32)->() = $callback;
        for i in 0..times {callback(i);}
    };
    (system($cmd:expr, $($arg:expr),*)) => {
        use std::process::Command;
        let mut values = Vec::new(); // vec 2.0
        $(values.push($arg);)*
        Command::new($cmd).args(values).spawn().unwrap().wait().unwrap();
    };
    (sleep($time:expr)) => {
        use std::thread::sleep;
        use std::time::Duration;
        sleep(Duration::from_secs($time));
    };
    (var $x:ident = $y:expr) => {
        let mut $x = $y; // value are always mutable
    };
}

#[macro_export]
macro_rules! eval_alot { //FIXME: Currently doesnt work.
    ($x:pat, $($y:pat),+) => {
        eval!($x)
        $(eval!($y);),+
    };
}

#[macro_export] 
macro_rules! define {
    () => {};
    ($name:ident, $value:expr) => {
        let mut $name = $value; 
    };
}
